import React, {Component} from "react";
import {Link} from "react-router-dom";
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddIcon from '@material-ui/icons/Add';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';

export class MenuDrawer extends Component {

  render() {
    return (
      <div>
          <React.Fragment key="isOpen">
              <Drawer anchor="left" open={this.props.drawerIsOpen} onClose={this.props.toggleDrawer(false)}>
                  <div
                    role="presentation"
                    onClick={this.props.toggleDrawer(false)}
                    onKeyDown={this.props.toggleDrawer(false)}
                  >
                    <List component="nav">
                      <ListItem button key="Manage" component={Link} to="/manage">
                        <ListItemIcon><SettingsIcon /></ListItemIcon>
                        <ListItemText primary="Manage"/>
                      </ListItem>
                      <ListItem button key="Display" component={Link} to="/display">
                        <ListItemIcon><DesktopWindowsIcon /></ListItemIcon>
                        <ListItemText primary="Display"/>
                      </ListItem>
                      <ListItem button key="LinkTwitch" component={Link} to="/linktwitch">
                        <ListItemIcon><AddIcon /></ListItemIcon>
                        <ListItemText primary="Link Twitch"/>
                      </ListItem>
                      <ListItem button key="LinkDevice" component={Link} to="/linkdevice">
                        <ListItemIcon><AddIcon /></ListItemIcon>
                        <ListItemText primary="Link Device"/>
                      </ListItem>
                    </List>
                    <Divider />
                    <List>
                      <ListItem button key="Logout" component={Link} to="/logout">
                        <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                        <ListItemText primary="Logout" />
                      </ListItem>
                    </List>
                  </div>
              </Drawer>
          </React.Fragment>
      </div>
    );
  }
}

export default MenuDrawer;
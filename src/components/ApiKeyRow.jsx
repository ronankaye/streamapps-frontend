import React, { Component } from "react";
import Button from 'react-bootstrap/Button';
import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import VisibilityOffRoundedIcon from '@material-ui/icons/VisibilityOffRounded';
import VisibilityRoundedIcon from '@material-ui/icons/VisibilityRounded';

class ApiKeyTable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isHidden: true,
    };

  }

  isHidden = (hide) => {
    this.setState({ isHidden: hide });
  }

  render() {
    return (
      <tr>
        <td>{this.props.apiKey.service} - {this.props.apiKey.apiUsername}</td>
        {this.state.isHidden ? <td>- - - - - - - - - - - - - - - - - - - - - - - - - - - -</td> : <td>{this.props.apiKey.key}</td>}
        <td>
          <center>
            <Button style={{ margin: '5px' }} variant="danger" size='sm' onClick={() => this.props.onDelete()}><DeleteForeverRoundedIcon/></Button>
            {this.state.isHidden ? <Button style={{ margin: '5px' }} variant="primary" size='sm' onClick={() => this.isHidden(false)}><VisibilityRoundedIcon/></Button> : <Button style={{ margin: '5px' }} variant="primary" size='sm' onClick={() => this.isHidden(true)}><VisibilityOffRoundedIcon/></Button>}
          </center>
        </td>
      </tr>
    );
  }

}

export default ApiKeyTable;